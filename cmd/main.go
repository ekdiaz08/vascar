package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	_ "github.com/joho/godotenv/autoload"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/ekdiaz08/vascar/internal/config"
	"gitlab.com/ekdiaz08/vascar/internal/db"
	httpRest "gitlab.com/ekdiaz08/vascar/internal/http"
	"gitlab.com/ekdiaz08/vascar/pkg/crud"
)

func main() {
	conf := config.NewConfig()
	sqlite, err := db.NewClientSqlite()
	if err != nil {
		log.Println("error with your connection")
		os.Exit(1)
	}

	crudRepository := crud.NewSqlite(sqlite.DB)
	crudService := crud.NewService(crudRepository)
	crudHandler := crud.NewHandler(crudService)
	httpTransportRouter := httpRest.NewHTTPRouter(crudHandler)

	srv := &http.Server{
		Addr: fmt.Sprintf("0.0.0.0:%s", conf.HttpPort),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      httpTransportRouter,
	}
	fmt.Println("transport", "http")
	fmt.Println("port", conf.HttpPort)
	fmt.Println("Transport Start")

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		fmt.Println("transport", "http")
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
		log.Printf("Transport Stopped")
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)
	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	_ = srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	fmt.Println("Service gracefully shut down")
	os.Exit(0)
}
