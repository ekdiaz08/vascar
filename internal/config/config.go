package config

import (
	"os"
)

const (
	port = "HTTP_PORT"
)

type config struct {
	HttpPort  string
	SqliteURL string
}

func NewConfig() config {
	return config{
		HttpPort: os.Getenv(port),
	}
}
