package http

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/ekdiaz08/vascar/pkg/crud"
)

func NewHTTPRouter(cr crud.Handler) http.Handler {
	r := chi.NewMux()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/post", cr.GetPostHandler)
	r.Post("/post", cr.RegisterPostHandler)
	r.Get("/comment/{id}", cr.GetByIDCommentHandler)
	r.Post("/comment", cr.RegisterCommentHandler)

	return r
}
