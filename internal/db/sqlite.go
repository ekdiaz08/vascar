package db

import "database/sql"

type Client struct {
	*sql.DB
}

func NewClientSqlite() (*Client, error) {
	db, err := sql.Open("sqlite3", "./identifier.sqlite")
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(1)

	return &Client{
		db,
	}, nil
}
