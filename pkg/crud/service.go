package crud

import (
	"context"
)

type Service interface {
	AddPost(ctx context.Context, req *Post) (id int, err error)
	AddComment(ctx context.Context, req *Comment) (id int, err error)

	GetPost(ctx context.Context) ([]Post, error)
	GetByIDComment(ctx context.Context, id int) (*PostComment, error)
}

type service struct {
	repo Repository
}

func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

func (s service) AddPost(ctx context.Context, req *Post) (id int, err error) {
	id, err = s.repo.AddPost(ctx, req)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (s service) AddComment(ctx context.Context, req *Comment) (id int, err error) {
	id, err = s.repo.AddComment(ctx, req)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (s service) GetPost(ctx context.Context) ([]Post, error) {
	post, err := s.repo.GetPost(ctx)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s service) GetByIDComment(ctx context.Context, id int) (*PostComment, error) {
	post, err := s.repo.GetByIDPost(ctx, id)
	if err != nil {
		return nil, err
	}

	comment, err := s.repo.GetByIDComment(ctx, post.ID)
	if err != nil {
		return nil, err
	}

	l := len(comment)
	rComment := make([]MyComment, l)
	for i := 0; i < l; i++ {
		c := comment[i]
		rComment[i] = MyComment{
			ID:        c.ID,
			Name:      c.Name,
			Reason:    c.Reason,
			CreatedAt: c.CreatedAt,
		}
	}
	result := &PostComment{
		Post: Post{
			ID:        post.ID,
			Name:      post.Name,
			Comment:   post.Comment,
			CreatedAt: post.CreatedAt,
		},
		Comment: rComment,
	}

	return result, nil
}
