package crud

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var ctx context.Context

type repositoryMock struct{}

func (r repositoryMock) AddPost(ctx context.Context, req *Post) (id int, err error) {
	panic("implement me")
}

func (r repositoryMock) AddComment(ctx context.Context, req *Comment) (id int, err error) {
	panic("implement me")
}

func (r repositoryMock) GetByIDPost(ctx context.Context, id int) (*Post, error) {
	panic("implement me")
}

func (r repositoryMock) GetPost(ctx context.Context) ([]Post, error) {
	panic("implement me")
}

func (r repositoryMock) GetByIDComment(ctx context.Context, postID int) ([]GetComment, error) {
	panic("implement me")
}

type addPostMockService struct {
	repositoryMock
	addPostRes int
	addPostErr error
}

func (a addPostMockService) AddPost(ctx context.Context, req *Post) (id int, err error) {
	return a.addPostRes, a.addPostErr
}

func TestService_AddPost(t *testing.T) {
	errMock := errors.New("not found")
	tests := []struct {
		name       string
		addPostReq *Post
		addPostRes int
		addPostErr error
		response   int
		err        error
	}{
		{
			name: "success",
			addPostReq: &Post{
				ID:      1,
				Name:    "ernesto",
				Comment: "only is example",
			},
			addPostRes: 1,
			addPostErr: nil,
			response:   1,
			err:        nil,
		},
		{
			name:       "fail",
			addPostReq: &Post{},
			addPostRes: 0,
			addPostErr: errMock,
			response:   0,
			err:        errMock,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			repoMock := addPostMockService{
				addPostRes: test.addPostRes,
				addPostErr: test.addPostErr,
			}

			ser := NewService(repoMock)
			resp, err := ser.AddPost(ctx, test.addPostReq)
			assert.Equal(t, test.response, resp)
			assert.Equal(t, test.err, err)
		})
	}

}

type addCommentMockService struct {
	repositoryMock
	addCommentRes int
	addCommentErr error
}

func (a addCommentMockService) AddComment(ctx context.Context, req *Comment) (id int, err error) {
	return a.addCommentRes, a.addCommentErr
}

func TestService_AddComment(t *testing.T) {
	errMock := errors.New("not found")
	tests := []struct {
		name          string
		addPostReq    *Comment
		addCommentRes int
		addCommentErr error
		response      int
		err           error
	}{
		{
			name: "success",
			addPostReq: &Comment{
				PostID: 1,
				Name:   "ernesto",
				Reason: "example",
			},
			addCommentRes: 1,
			addCommentErr: nil,
			response:      1,
			err:           nil,
		},
		{
			name:          "fail",
			addPostReq:    &Comment{},
			addCommentRes: 0,
			addCommentErr: errMock,
			response:      0,
			err:           errMock,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			repoMock := addCommentMockService{
				addCommentRes: test.addCommentRes,
				addCommentErr: test.addCommentErr,
			}

			ser := NewService(repoMock)
			resp, err := ser.AddComment(ctx, test.addPostReq)
			assert.Equal(t, test.response, resp)
			assert.Equal(t, test.err, err)
		})
	}
}

type getPostMockService struct {
	repositoryMock
	getPostRes []Post
	getPostErr error
}

func (g getPostMockService) GetPost(ctx context.Context) ([]Post, error) {
	return g.getPostRes, g.getPostErr
}

func TestService_GetPost(t *testing.T) {
	getPost := []Post{
		{
			ID:      1,
			Name:    "test",
			Comment: "only is test",
		},
	}
	errMock := errors.New("not found")
	tests := []struct {
		name       string
		getPostRes []Post
		getPostErr error
		response   []Post
		err        error
	}{
		{
			name:       "success",
			getPostRes: getPost,
			getPostErr: nil,
			response:   getPost,
			err:        nil,
		},
		{
			name:       "fail",
			getPostRes: []Post{},
			getPostErr: errMock,
			response:   nil,
			err:        errMock,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			repoMock := getPostMockService{
				getPostRes: test.getPostRes,
				getPostErr: test.getPostErr,
			}

			ser := NewService(repoMock)
			resp, err := ser.GetPost(ctx)
			assert.Equal(t, test.response, resp)
			assert.Equal(t, test.err, err)
		})
	}
}

type getByIDCommentMockService struct {
	repositoryMock
	getPostRes         *Post
	getPostErr         error
	getByIDCommentResp []GetComment
	getByIDCommentErr  error
}

func (g getByIDCommentMockService) GetByIDPost(ctx context.Context, id int) (*Post, error) {
	return g.getPostRes, g.getPostErr
}

func (g getByIDCommentMockService) GetByIDComment(ctx context.Context, postID int) ([]GetComment, error) {
	return g.getByIDCommentResp, g.getByIDCommentErr
}

func TestService_GetByIDComment(t *testing.T) {
	errMock := errors.New("not found")
	getByID := 1
	getPost := []MyComment{
		{
			ID:     1,
			Name:   "test",
			Reason: "only is test",
		},
	}
	tests := []struct {
		name               string
		getByIDCommentReq  int
		getPostRes         *Post
		getPostErr         error
		getByIDCommentResp []GetComment
		getByIDCommentErr  error
		response           *PostComment
		err                error
	}{
		{
			name:              "success",
			getByIDCommentReq: getByID,
			getPostRes: &Post{
				ID:      1,
				Name:    "test",
				Comment: "only is test",
			},
			getPostErr: nil,
			getByIDCommentResp: []GetComment{
				{
					ID:     1,
					Name:   "test",
					Reason: "only is test",
				},
			},
			getByIDCommentErr: nil,
			response: &PostComment{
				Post: Post{
					ID:      1,
					Name:    "test",
					Comment: "only is test",
				},
				Comment: getPost,
			},
			err: nil,
		},
		{
			name:               "fail",
			getByIDCommentReq:  getByID,
			getPostRes:         &Post{},
			getPostErr:         errMock,
			getByIDCommentResp: []GetComment{},
			getByIDCommentErr:  errMock,
			response:           nil,
			err:                errMock,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			repoMock := getByIDCommentMockService{
				getPostRes:         test.getPostRes,
				getPostErr:         test.getPostErr,
				getByIDCommentResp: test.getByIDCommentResp,
				getByIDCommentErr:  test.getByIDCommentErr,
			}

			ser := NewService(repoMock)
			resp, err := ser.GetByIDComment(ctx, test.getByIDCommentReq)
			assert.Equal(t, test.response, resp)
			assert.Equal(t, test.err, err)
		})
	}
}
