package crud

type Post struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Comment   string `json:"comment"`
	CreatedAt string `json:"created_at"`
}

type Comment struct {
	PostID int    `json:"post_id"`
	Name   string `json:"name"`
	Reason string `json:"reason"`
}

type GetComment struct {
	ID        int
	Name      string
	Reason    string
	CreatedAt string
}
