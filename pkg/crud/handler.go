package crud

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/ekdiaz08/vascar/internal/response"
)

type Handler struct {
	serv Service
}

func NewHandler(serv Service) Handler {
	return Handler{
		serv: serv,
	}
}

func (h *Handler) RegisterPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var post Post
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	err := d.Decode(&post)
	if err != nil {
		_ = response.RespondWithError(w, response.StandardBadBodyRequest)
		return
	}

	defer r.Body.Close()

	id, err := h.serv.AddPost(ctx, &post)
	if err != nil {
		_ = response.RespondWithError(w, response.StandardInternalServerError)
		return
	}

	_ = response.RespondWithData(w, http.StatusCreated, AddPost{
		ID: id,
	})
}

func (h *Handler) RegisterCommentHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var comment Comment
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	err := d.Decode(&comment)
	if err != nil {
		_ = response.RespondWithError(w, response.StandardBadBodyRequest)
		return
	}

	defer r.Body.Close()

	id, err := h.serv.AddComment(ctx, &comment)
	if err != nil {
		_ = response.RespondWithError(w, response.StandardInternalServerError)
		return
	}

	_ = response.RespondWithData(w, http.StatusCreated, AddComment{
		ID: id,
	})
}

func (h *Handler) GetByIDCommentHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	id, _ := strconv.Atoi(r.URL.Query().Get("id"))

	comment, err := h.serv.GetByIDComment(ctx, id)
	if err != nil {
		_ = response.RespondWithError(w, response.Error{
			Code:        response.ErrCodeNotFound,
			Description: err.Error(),
		})
		return
	}

	_ = response.RespondWithData(w, http.StatusOK, comment)
}

func (h *Handler) GetPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	posts, err := h.serv.GetPost(ctx)
	if err != nil {
		_ = response.RespondWithError(w, response.StandardNotFoundError)
		return
	}

	_ = response.RespondWithData(w, http.StatusOK, posts)
}
