package crud

type AddPost struct {
	ID int
}

type AddComment struct {
	ID int
}

type MyComment struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Reason    string `json:"reason"`
	CreatedAt string `json:"created_at"`
}

type PostComment struct {
	Post    Post
	Comment []MyComment
}
