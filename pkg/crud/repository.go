package crud

import (
	"context"
	"database/sql"
	"log"
)

type Repository interface {
	AddPost(ctx context.Context, req *Post) (id int, err error)
	AddComment(ctx context.Context, req *Comment) (id int, err error)
	GetByIDPost(ctx context.Context, id int) (*Post, error)
	GetPost(ctx context.Context) ([]Post, error)
	GetByIDComment(ctx context.Context, postID int) ([]GetComment, error)
}

type sqlite struct {
	db *sql.DB
}

func NewSqlite(db *sql.DB) Repository {
	return &sqlite{
		db: db,
	}
}

func (s sqlite) AddPost(ctx context.Context, req *Post) (id int, err error) {
	q := `INSERT INTO post(name, comment) VALUES (?, ?) returning id`

	stmt, err := s.db.PrepareContext(ctx, q)
	if err != nil {
		return 0, err
	}

	defer stmt.Close()

	err = stmt.QueryRowContext(ctx,
		req.Name,
		req.Comment,
	).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (s sqlite) AddComment(ctx context.Context, req *Comment) (id int, err error) {
	q := `insert into comment(post_id, name, reason) VALUES (?, ?, ?) returning id`

	stmt, err := s.db.PrepareContext(ctx, q)
	if err != nil {
		log.Fatal(err)
		return 0, err
	}

	defer stmt.Close()

	err = stmt.QueryRowContext(ctx,
		req.PostID,
		req.Name,
		req.Reason,
	).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (s sqlite) GetByIDPost(ctx context.Context, id int) (*Post, error) {
	q := `SELECT id, "name", comment, created_at FROM post`
	var res Post
	err := s.db.QueryRowContext(ctx, q, id).Scan(
		&res.ID,
		&res.Name,
		&res.Comment,
		&res.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (s *sqlite) GetPost(ctx context.Context) ([]Post, error) {
	q := `select id, "name", comment, created_at from post;`

	res := make([]Post, 0)
	row, err := s.db.QueryContext(ctx, q)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		var (
			p         Post
			createdAt sql.NullString
		)

		err := row.Scan(
			&p.ID,
			&p.Name,
			&p.Comment,
			&createdAt,
		)
		if err != nil {
			continue
		}
		p.CreatedAt = createdAt.String
		res = append(res, p)
	}

	return res, nil
}

func (s sqlite) GetByIDComment(ctx context.Context, postID int) ([]GetComment, error) {
	q := `SELECT c.id, c.name, c.reason, c.created_at
		FROM comment c inner join post p on p.id = c.post_id where p.id = 1;`

	res := make([]GetComment, 0)
	rows, err := s.db.QueryContext(ctx, q, postID)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			p         GetComment
			createdAt sql.NullString
		)

		err := rows.Scan(
			&p.ID,
			&p.Name,
			&p.Reason,
			&createdAt,
		)
		if err != nil {
			continue
		}
		p.CreatedAt = createdAt.String
		res = append(res, p)
	}

	return res, nil
}
