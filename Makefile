
init:
	docker compose -f docker-compose.yml up -d;
down:
	docker compose down

test:
	@go test -cover ./...